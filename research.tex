\subsection{Data Structures}
In the past few years,
there have been many proposals for highly concurrent implementations of important,
widely-used data structures such as queues~\cite{MichaelS1996},
linked lists~\cite{Harris2001},
skiplists~\cite{HerlihyLLVS07,skiplist,skiplist1},
hash tables~\cite{ShalevShavit,springerlink:10.1007},
and others~\cite{HerlihyS2008}.
The proposed project will make a similar effort with respect to durability.
It is well-established that effective fine-grained synchronization depends on
a balance between techniques specific to the
particular data structure in question,
and general-purpose mechanisms such as scalable spin-locks.
In the same way,
we anticipate that adding durability to existing highly-concurrent
data structures will require a similar balance between customized,
type-specific techniques, and more general-purpose mechanisms.

\subsection{Correctness}
For a sequential data structure,
one might require that the effects of all operations completed before
a crash be recovered after the crash,
while the effects of an operation interrupted by the crash may or may
not be recovered,
a property known as \emph{durable linearizability}~\cite{IMS2016}.
This correctens condition, while easy to describe,
seems to require flushing the contents of volatile caches
back to NVM at every opportunity,
which is likely to be unacceptably expensive for many applications.

Despite its practical limitations,
it is still instructive to consider some of the implications.
of adopting durable linearizability as a correctness condition.
Even for sequential objects,
durable linearizability presents some implementation challenges.
if the interrupted operation left the non-volatile part of the data
structure in an inconsistent state,
then the object must provide a recovery method to detect and fix
such inconsistencies,
either completing or discarding the effects of the interrupted operation,
It makes little sense to talk of adding durability to a data structure
without specifying what, precisely, durability means.
The question becomes more complex for concurrent objects where a crash
might interrupt multiple operations on the same object.
Here, too, the interrupted operations may have left the data structure
in a inconsistent intermediate state.
The recovery method is not free to complete or discard interrupted
operations arbitrarily,
because there may be dependencies linking completed and interrupted
operations.
For example,
consider a durably linearizable stack where \var{push(x)} and
\var{pop()} calls overlap.
The \var{pop()} call returns \var{x},
and immediately after, a crash occurs before \var{push(x)} returns.
Because the \var{pop()} completed, its effects survive,
but because the \var{pop()} depends on \var{push(x)}
(it is linearized after), the \var{push(x)} must survive as well.

There is an intriguing and largely unexplored connection between
durability and synchronization.
Lock-free concurrent data structures seem inherently easier to render
durable than lock-based structures.
Informally,
a lock-free data structure is always in a consistent state,
in the sense that if any thread halts in the middle of an operation,
the remaining threads can complete their operations without the need
to restore any kind of invariants.
By contrast, in a lock-based concurrent data structure,
if a thread halts in the middle of a critical section,
the data structure itself is typically inconsistent
(which is why mutual exclusion was needed in the first place).
In the same way,
restarting a durably linearizable lock-free data structure following a
crash should require little or no recovery,
while restarting a lock-based data structure is more likely to require
non-trivial work to restore the structure to a consistent state.
This observation, is, of course, informal.
Making observations like this one precise,
understanding their limits,
and deriving practical applications is one of the principal reseach
goals of the proposed project

Having established that implementing even such a simplistic
and apparantly inefficient property as durable linearizability raises
a number of non-trivial questions,
we turn our attention to more complex,
but perhaps more practical correctness conditions.
In \emph{buffered durable linearizability}~\cite{IMS2016},
the effects of completed operations may be lost after a crash,
but dependency relations among surviving operations are preserved.
For example,
if a \var{push(x)} operation is applied to a stack,
and a concurrent or later \var{pop()} operation returns \var{x},
then if the \var{pop()} survives a later crash, so does the
\var{push(x)}, but it is possible that only the \var{push(x)}
operation survives, or that neither survives.
This property is attractive because it seem to require fewer expensive
flushes, suggesting a more general trade-off:
less frequent flushes could provide better performance in the normal case,
but might make crash recovery more complex because of weaker gurantees
about which operations' effects survive.
Elsewhere~\cite{FriedmanHP2017}, we describe a durably linearizable
lock-free queue,
but in general, little is known about effective techniques for
implementing durable linearizability for other data types,
or about efficiency-complexity trade-offs for durability in general.

A data structure provides \emph{detectable executions} if it is possible to
tell at the end of a recovery phase whether the effects of a specific operation
survived the crash.
In prior work~\cite{FriedmanHP2017},
we propose a durable queue implementation that provides detectable executions
structures.
In general, however, it is an open question how to provide detectable
executions for other data structures,
and little is known about how the cost of providing this property
varies with the correctness condition.
For example, adding detectable executions to buffered durable
linearizability may incur costs because it is necessary to keep track
of many operations,
partially cancelling the savings from reducing the number of cache
flushes.
It is possible that effective detectable executions require
type-specific techniques, similar to type-specific synchronization,
or there may be general-purpose techniques, such as write-ahead
logging, that work effectively for all data structures.

There are many open questions concerning the flush primitives needed to
move data from volatile caches to NVM.
A natural place to start is to assume that existing cache flush
instructions,
such as CFLUSH and SFENCE currently provided on Intel architectures to
flush from cache to (volatile) memory,
will also be used for NVM.
A key challenge in developing the durable concurrent data stuctures
mentioned earlier is to decide where to place such flush instructions.
Performance suffers if there are too many,
and correctness suffers if there are too few.
Although there is a suggestive analogy between NVM flushes
and \emph{memory fence} operations  used to flush updates from store buffers to caches,
they are not quite the same thing.
Today, expert programmers can place memory barriers by hand,
but usually they are placed by compilers,
typically at the boundaries of critical sections.
The proposed project will investigate the proper and effective use of
NVM flush instructions, with attention to having compilers place them
automatically.

As a result of investigating these questions,
the project may gain insight into whether simple hardware changes
might provide more useful or more powerful forms of NVM flush.
For example, it may be convenient to be able to flush multiple cache
lines atomically.
It may even be useful to temporarily inhibit flushing one or more
cache lines while a complex operation is in progress,
in much the same way that databases can pin pages in memory to keep
them from being written back to disk.

Since NVM flushes are expected to contribute the most to the costs of
durable data structure operation,
it is natural to ask whether one analyze the inherent costs,
expressed as NVM flushes,
needed to implement various data structures.
By analogy, there is a well-developed theory bounding from above and below
the number of \emph{remote memory references} needed for various kinds of mutual
exclusion~\cite{AttiyaHW2008}.
The proposed project will develop a formal model and investigate both
upper and lower bounds for the number of NVM flushes needed for
various concurrent data structures.

In a different span of these questions and challenges, one may ask what additional properties can be obtained if one is willing to spend more execution time due by the additional flushes. For example, if full durable linearizability is required, and a specific algorithm requires an abundance of flush operations, then execution time becomes longer. And when the execution becomes long, some properties that beforehand seemed too costly to hope for, become more applicable in light of the already longer execution time. The best example for such a property is detectable execution described above. Adding a logging mechnism to achieve detectable execution with a standard lock-free queue  incurs a very high overhead in practice. But if full durable linearization for the queue reduces its performance, then detectable execution becomes reasonable, almost negligble, in the sense that the percent overhead is a lot smaller~\cite{FriedmanHP2017}. What other properties are reasonable to implement when the execution time becomes longer? This is another question that we propose to study in this project. 


\begin{itemize}
\item compositionality and correctness conditions that span multiple objects
\item safety vs liveness
\item automatic transformations of conventional to durable: how general?
\item formal models for durable objects and transactions
\end{itemize}

\subsection{Memory Management}

Non-volatile memories raise interesting challenges for memory management as well. The most obvious challenge is to make sure that objects are not leaked when a crash occurs. If we attempt to let a program continue executing after a crash, then leaked memory may create a significant problem. In current volatile memory, a crash causes the entire memory to be wiped out and the memory management to start from scratch with an assumed clean memory (and no memory resilience). This ensures that all the memory is always available. But suppose a program is executed on non-colatile memory and a crash creates inconsistecy with the memory manager's meta data. This may cause parts of thr memory to seem like they were allocated for program's use, even though the program is not aware of this allocation and is not using these spaces. In this case, these parts of the memory are "lost" and it will not be possible to use them again. This loss of mmeory is denoted a {\em memory leak}. Note that with volatile memory this cannot happen, as all memory becomes available after a crash.  

Memory leaks do not have to involve huge spaces, and do not neecessarily stem from corrupted meta-data. A different cause of a memory leak can be an allocation procedure that occurs concurrently with a crash. Consider the point in the execution when the allocator delivers an allocated area to the program and assumes that from there on that the program is using that space until the it reports this space free. If a crash occurs at this point, it is possible that the program has not yet received posession of this space. From that point on, this space does not have an owner that may free it for reuse and it will never be used again. While this space may be smaller or larger, it is lost forever, because there is no mechanism that recovers this space. As crashes occur repeatedly in the course of time, the accumulated amount of space that leaks becomes larger and larger until the execution is rendered very slow or it becomes impossible at all. 
In this project we porpose to design a memory manager that avoids memory leaks due to crashes. 

Memory management for concurrent algorithms and data structure is notoriously challenging~(e.g., \cite{HP,herlihy2005nonblocking, detlefs2002lock, dropta, st14, citeoa}). Adding resilience makes the problem more challenging. We propose to study making available methods resilient to crashes for non-volatile memories. This involves adapting known methods such as hazard pointers, epoch-based reclamation, etc. resilient to crashes. Next, we plan to study new resislient allocation algorithms that are especially tailored for concurrent executions on non-volatile memories. 

Going further, we propose to look at automatic memory managers, i.e., on garbage collectors and their matching allocators. As a first goal, we would like to understand which garbage collectors are adequate and most effective in this environment. All meta-data needs to be adapted to crash scenarios and allow recovery with no memory leakage. In addition, the collector must be adapted to ensure that in the face of crashes during a garbage collection, it is possible to re-execute the collection with no lingering bad effects. 
A more advanced challenge is to design a new garbage collector (and matching alloator) that are most adeqaute for the non-volatile memories, with crash resilience and effectiveness (high performance and short pauses) for concurrently executing threads. 

An initial promising research has recently appeared in~\cite{BCB2016}. They presented the Makalu allocator whose meta-data is carefully monitored to properly suvive crashes. They use an additional specially designed garbage collector whose purpose is to detect and corret memory leaks that might follow a crash. 

\begin{itemize}

\item make sure memory not lost or corrupted
\item complex because form of consistency that spans multiple objects
\item explicit: malloc() and free()
\item also: garbage collection  
\end{itemize}

\subsection{Synchronization}
\begin{itemize}
\item lock-free
\item lock-based
\item transactional (see multi-object correctness)
  \begin{itemize}
  \item HW vs SW transactions
  \item are durability transactions same granularity as synchronization transactions?
  \end{itemize}
\item flat combining and related methods
\end{itemize}


