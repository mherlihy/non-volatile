% This is the main project description file.
% Sections should be moved to separate files to enable concurrent editing
\documentclass[12pt]{article}
\usepackage{listings}
\usepackage{color} 

\usepackage{url}
\usepackage{titlesec}
\titleformat{\section}{\large\bfseries}{\thesection}{1em}{}
\newcommand{\remove}[1]{}
\newcommand{\todo}[1]{\marginpar{\small \emph{todo:} #1}}
\newcommand\todoinline[1]{\noindent{\ \\\fbox{\begin{minipage}{4.5in} TODO: #1 \end{minipage}}}}
%
\newcommand\blue[1]{{\color{blue} #1}}
\newcommand\red[1]{{\color{red} #1}}
\newcommand{\ignore}[1]{}

\newcommand{\var}[1]{\lstinline+#1+}

\newcounter{enote_counter}
\newcounter{mnote_counter}

\newcommand{\enote}[1]{
\addtocounter{enote_counter}{1}
\begin{quote}{\bf Erez's note \arabic{enote_counter}:} #1\end{quote} }
\newcommand{\desc}[1]{{\bf #1}}

\newcommand{\mnote}[1]{
\begin{quote}{\bf Maurice's note \arabic{mnote_counter}:} #1\end{quote} \addtocounter{mnote_counter}{1}}



%\usepackage[top=2.5cm,bottom=2.5cm,left=2.5cm,right=2.5cm]{geometry}
\usepackage[margin=3.7cm]{geometry}


\title{\Large\bf BSF: xxxx: Collaborative Research:\\
Durable Data Structures and Algorithms for Non-Volatile Main Memory
\\
$\;$\\ Research Plan}
\author{Maurice Herlihy \\ {\small Brown University}
\and Erez Petrank\\ {\small Technion}}

\date{}
\linespread{1.3}

\begin{document}
\maketitle

\section{Abstract}
Non-volatile memory is expected to displace volatile DRAM for main memory
(but not caches or registers) in upcoming architectures.
As a result,
there is increasing interest in the problem of designing and specifying \emph{durable} data structures that can recover from system crashes. 
In this research proposal we propose to design and implement a variety of basic 
data structures that can recover from a crash. We focus on {\emph{concurrent} data structures
and plan to explore the complexity of obtaining 
various levels of guarantees on data preservation during a crash. 


\section{Introduction}
Memory is said to be \emph{non-volatile} if it does not lose its contents after a system crash.
Non-volatile memory is widely used today, e.g., in SSD replacements for hard disks. 
Hardware for non-volatile memory is advancing fast and soon it is expected to be fast and 
dense enough to be able to displace volatile DRAM for main memory in many architectures. 
However, due to hardware
wear and other hardware constraints it is not expected to displace caches and registers.
The proposed research is about developing software that can utilize the ability of non-volatile
memory to recover off a crash. 

For a system with volatile memory, the entire memory is lost during a crash, and it is therefore
important to write to disk consistent versions frequently. However, for an architecture 
in which the main memory is non-volatile, it is possible to recover from a crash and start using the 
memory again with its priori-crash content. Many challenges exist for such recoveries, all of which 
are being broadly investigated by a many researchers. In this proposal, we focus on making data structures
recoverable. Furthermore, we focus on concurrent data structure, which is within the expertise of the principle researchers. 
 
We propose to design, implement, and report performance of \emph{durable} data structures, that is, data structures whose state can be recovered after a system crash.
As noted,
caches and registers are expected to remain volatile,
so the state of main memory following a crash may be inconsistent,
requiring non-trivial algorithms to restore the data structure to a consistent state.

Moreover, careful thought is needed to define what it \emph{should mean} for a object to be durable.
For example, can the effects of operations in progress at the time of the crash be lost?
What about operations that completed before the crash,
or dependencies that span multiple objects?

In the absence of durability concerns,
\emph{linearizability}~\cite{HW90} is perhaps the most commonly-used
correctness condition for concurrent objects:
each operation appears to ``take effect'' instantaneously at some point between its 
invocation and response.
Linearizability is  attractive because (unlike, say, sequential consistency),
it is \emph{compositional}:
the composition of two linearizable objects is itself linearizable. This has the
important implication that one can prove linearizability of an object independently. 
There is no need to show correctness for each possible interaction between objects in the system. 

Recently,
Izraelevitz \emph{et al.}~\cite{IMS2016} proposed \emph{durable
linearizability} as a natural way to extend linearizability to durable data
structures. 
Informally,
durable linearizability guarantees that the state of the object following a
crash reflects a consistent subhistory of the operations that actually occurred.
This subhistory includes all operations that completed before the crash,
and may or may not include operations in progress when the crash occurred.
It is not hard to see that durable linearizability is compositional:
the composition of two durably linearizable objects is itself durably
linearizable.
Nevertheless, durable linearizability can be expensive,
requiring frequent \emph{persistence barriers}:
forcing data from volatile caches into non-volatile main memory can take
hundreds of cycles.
Izraelevitz \emph{et al.} describe a systematic way to transform a lock-free
linearizable data structure into a durably linearizable data structure by
associating persistence barriers with most memory accesses.


Recongizing that such frequent persistence barriers could be expensive,
Izraelevitz \emph{et al.} propose an alternative, weaker condition,
called \emph{buffered durable linearizability}.
Informally,
this condition guarantees that the state of the object following a
crash reflects a consistent subhistory of the operations that actually
occurred,
but this subhistory \emph{need not} include all operations that completed before the crash.
Buffered durable linearizability is potentially more efficient than its
unbuffered counterpart, because it does not require such frequent persistence fences.

Unfortunately,
buffered durable linearizability is not compositional:
the composition of two buffered durably linearizable objects is itself not
buffered durably linearizable.
For example,
suppose a thread dequeues value $x$ from $p$,
and then enqueues $x$ on $q$,
where $p$ and $q$ are distinct buffered durably linearizable FIFO queues.
Following a crash,
the thread might find two copies of $x$, one in each queue,
while if the composition of the two queeus were buffered durably linearizable,
the recovering thread might find $x$ in $p$ but not $q$, or in $q$ but not $p$,
or in neither, but never in both.

Durable objects and libraries are unlikely to be useful if they cannot be composed,
since it is essentially impossible for applications to guarantee invariants
that span multiple objects, or to preserve dependencies between objects.
Thus, such objects are expected to provide 
a linearizable operation, \var{sync()},
that forces a single-object persistence barrier:
a call to an object's \var{sync()} method renders durable all that object's
operations completed before the call,
although operations concurrently with the call may or may not be rendered durable.

\enote{Should we survey older work that considered threads crashing independently of each other? }





The goal of this proposal is to foster cooperation between two ongoing
projects, one, lead by PI Herlihy at Brown University,
and the other led by PI Petrank at the Technion. 

PI Herlihy's ongoing project centers around rethinking and redesigning basic
synchronization structures such as locks, 
flat combining, and a range of concurrent data structures
such as heaps, hash tables, and skip lists.
These new structures will be embodied in an open-source C++ library. PI Herlihy will also work on the theoretical foundations for composing low-cost durable algorithms. He will work on establishing a trade-off between the levels of durability duarantees and the performance costs that they entail. 

PI Petrank's ongoing project focuses on new non-blocking concurrent algorithms for fundamental data structures, such as lock-free B-trees, 
wait-free queues, wait-free linked-lists, and others. PI Petrank also has an 
ongoing research agenda in memory management at large and in particular, he is exploring 
how durability guarantees and progress guarantees,
(such as those provided by lock-free or wait-free concurrent algorithms)
interact with memory management. PI Petrank is also interested in extending the resilience support for runtimes and systems, which is highly challenging. 

We see this proposal as a unique opportunity to establish substantial
cooperation between these two projects, and the expertise gained in both to aim at adequate
software that adequate for multicores execution on platforms with non-volatile memories. 
The grant will allow us to support students to work on topics of mutual interest,
and to exchange researchers between the two groups.
In particular, PI Petrank's expertise in practical wait-free data structures
and memory management is an ideal complement to PI Herlihy's expertise in
transactional memory and related models of synchronization.

We have several goals, mainly aimed at exploring software design for non-volatile memories with a focus on concurrent data structures and memory management. 
\enote{Should we also state transactions?}
 issues we proposed to explore include:
\begin{itemize}
\item Design, implement. and measure the performance of durable versions for important, widely-used, concurrent data structures, such as queues, linked lists, skip lists, hash tables, etc.
\item Explore the cost of obtaining various levels of durability guarantees.
\item Explore the relation between progress guarantees are durable guarantees. 
\item Explore the cost ofmanual  memory management and propose avenues for improving memory management performance.  
\item Explore the cost of automatic memory amanagement (with garbage collection) in this setting. 
\item Study durable transacitons (both software and those building on hardware transactional memories). 
Compare the above implementations with durable transactions, used to implement similar data structures. 
\item 
Design a general construction that automatically transforms a set of data structures implementations into implementations that provide durability. 
\item 
Design locking mechanisms that would be approrate for use with durable fuarantees. 
\enote{Mention ATLAS as an initial attempt}
\item
Add durability to traditional synchronization methods, with a focus on teh widely-used  flat combining method. 
\item
Study plugging and compositionality of durable data strcutures. Some definitions provide compositionality, but they are typically costly. Other definitions provide lower durabiity guarantees in order to improve performance. But they often do not provide copositionality. We would like to find ways to deal with this problem without harming performance. 
\item 
In particular, we will explore tools for composing buffered durability
\item
Explore models of computation that describe the new architectures and setting, and allow theoretical studies and arguing about the correctness and performance of platfroms with non-volatile memories. 
\item 
Study the theoretical question about how mmuch flushing is reqired to obtain compositionality. It is desirable to reduce flushing as much as possible but preserve linerizability as appropriate in this setting. 
\item
Study support for general runtime support. This includes memory management and synchronozation mechanisms as mentioned before but also issues relate to Just-In-Time compilation, specific runtime handling of synchronization structures, etc. 
\item
Study resilience of various system services. 
\item
The holy grail: study the possibility to provide full system resilience for platfroms with non-volatile memory. This is probably too much to acchieve in a single funded research, but we hope to lay some ground to enable subsequent research on this topic. 
\item
Build a solid evaluation environment, by identifying and proposing adequate nechmarks to evaluate performance on such platforms.  \item 
COnstruct simulators for potential variants of future architectures with non-volatile memory, on which we can run evaluation and measureme performance  of the various proposed methods.  
\item 
Crashing models?
\end{itemize}



This proposal is structured as follows.
In Section~\ref{sec:background},
we describe the hardware trends that are expected to lead to new platforms and the challenges 
that software must deal with in order to make good use of these new platforms. 

In Section~\ref{sec:ds},
we describe avenues to design concurrent data structures and properties of them, such as 
progress guarantees. 

In Section~\ref{sec:memory},
we describe the challenges presented by automatic, or semi-automatic memory
management in the context of highly-concurrent data structures on platforms with 
non-volatile memories.
It is well-known that conventional memory management techniques,
such as system-wide garbage collection or reference counting,
present challenges when applied to concurrent systems.
We propose to investigate a number of novel, object-local,
semi-automatic memory management techniques.

In the remaining sections, we discuss the merits and impact of the proposal,
the results of prior NSF support, and how we plan to collaborate.

Note that much (not all) of the work described here is already being pursued
independently by the PIs.
The goal of this proposal is bring these projects together and to enhance each
effort with closer cooperation.

\section{Background: Why Volatile Memories Create a New Challenge for Software}
\label{sec:background}

\subsection{Hardware Progress}
\subsection{Challenges for Software Support}


\section{The rest is our previous proposal to BSF. Maybe reuse parts?}

\section{Concurrent Data Structures and Progress Guarantees}
\label{sec:ds}
A method is \emph{lock-free} if it guarantees that \emph{some} thread
makes progress even if threads can halt or delay at arbitrary places in the code,
and a method is \emph{wait-free} if it guarantees that \emph{all} non-halted
threads make progress.
PI Petrank has recently been studying algorithmic construction 
of concurrent data structures with 
progress guarantees \cite{
DBLP:conf/pldi/PetrankMS09,
DBLP:conf/icdcn/BraginskyP11,
DBLP:conf/ppopp/KoganP12,
DBLP:conf/ppopp/TimnatBKP12,
DBLP:conf/spaa/BraginskyP12,
timn12a}.
For a long time,
many researchers have focused on lock-free data structures and algorithms,
assuming that the wait-free progress guarantee is too difficult to implement in
a practical way.
PI Petrank has done extensive prior work to change this assumption,
looking at practical wait-free data structures
such as linked lists~\cite{DBLP:conf/ppopp/TimnatBKP12,timn12a} and
queues~\cite{DBLP:conf/ppopp/KoganP11,DBLP:conf/ppopp/KoganP12}. 
PI Petrank's work has shown that one can construct practical wait-free data
structures by providing a lock-free ``fast-path'',
falling back to the wait-free path only when 
necessary~\cite{DBLP:conf/ppopp/KoganP12,timn12a}.


We think that a similar approach will be helpful in 
dealing with the \emph{best-effort} nature of hardware
transactions: the architecture typically provides no guarantee
that any particular transaction will commit.
The limited nature of hardware transactional memory's progress guarantees
is explained in the Intel Transactional Extensions documentation~\cite{IntelISE}. 
A transaction may fail because it is \emph{too large},
accessing so much data that internal buffers or caches overflow.
A transaction may fail because it takes \emph{too long},
and is always interrupted by the operating system schedule.
Transactions may fail if contention is high,
or, indeed, for arbitrary reasons.
In practice, of course, we expect most transactions to succeed,
but we must be prepared for any particular transaction to fail.

Here, we think that PI Petrank's strategy of efficiently guaranteeing progress through
multiple paths is a promising way to address the question
one can design efficient software data structures that provide progress
guarantees on top of hardware that provides no such guarantees.

A second promising research area is how one can streamline existing wait-free
and lock-free data structures to exploit the enhanced efficiency provided by
hardware transactional memory.
The best-effort nature of the hardware implies that one should avoid using
transactions that have potentially unbounded data sets or running times.
For example,
instead of using a single transaction to traverse a multi-node data structure
such as a list or a tree,
it makes sense to split the traversal into multiple, fixed-size transactions.
(Naturally, this is sometimes easier said than done.)
How large should these transactions be?
The most effective transaction size depends on the hardware resources available,
and on the current level of contention.
Hardware resources will vary from platform to platform,
and contention levels will vary from time to time.
Whenever possible,
it makes sense to choose transaction size dynamically.
If transactions of a certain size tend to fail,
then it is sensible to try shorter transactions, and vice-versa.
In the limit,
there must be a non-transactional fall-back path when the transactional path
repeatedly fails.

PI Herlihy's earlier work on concurrent skip-lists
\cite{Herlihy:2007:SOS:1760631.1760646},
devised an algorithm in which
lookups traverse the structure without acquiring locks,
while methods that add and remove items must acquire locks.
The key to this lock-based algorithm is the combination of two complementary
techniques.
First, it is optimistic: the methods traverse the list without acquiring
locks. Moreover, they are able to ignore locks taken by other threads while the
list is being traversed.
Only  when a method discovers the items it is seeking, does it lock the item
and its predecessors, and then validates that the list is unchanged.
Second, our algorithm is lazy: removing an item involves logically deleting it
by marking it before it is physically removed (unlinked) from the list.
Locks must be acquired in a particular order to ensure that concurrent
traversals interact correctly with methods that modify the data structure.

Hardware transactions can greatly simplify this kind of design.
A read-only transaction that traverses the data structure,
if it commits, is guaranteed to have seen a consistent state,
reducing or eliminating the need for validation.
If locks are acquired (or released) inside a transaction,
then there is no need for subtle reasoning about the order in which the locks
are acquired.
There is still, of course, a need for careful validation,
but the style and nature of the reasoning is quite different.

The ability to make atomic changes to multiple locations means that classical
data structures can be extended to provide richer sets of operations.
For example,
most concurrent min-heap implementations do not provide the ability to merge
two heaps together,
because such an operation is simply too complicated using conventional
synchronization.
Hardware transactions, however, can simplify this kind of task,
and we will investigate these issues as part of our library development.

Progress guarantees are also important for lock-based data structures.
One way to address this issue is to have objects manage their own thread-level
scheduling.
For example,
in the \emph{fork-join} model,
popularized by Cilk~\cite{Blumofe:1995:CEM:209936.209958},
makes the programmer responsible for exposing the parallelism and identifying
elements that can safely be executed in parallel.
The run-time environment, and particularly the scheduler,
decides during the execution how to divide the work between processors.
The computation is divided up into short tasks,
which can spawn other tasks,
and scheduling tasks on running threads is done by a work-stealing mechanism:
each thread in the pool of threads has a set of tasks to work on,
which it holds in a stealable queue.
A thread that is out of work attempts to \emph{steal} work from a thread that
has tasks waiting to be done.
 As the tasks are usually small,
there is typically much more potential parallelism than there are available threads,
and the way to make sure that the program scales efficiently is to keep the
threads as busy as possible.  

We will investigate how this model can be adapted to encompass both locks and
hardware transactions.
When a thread picks up a task and starts working on it,
it may find that the task needs a lock that is not available.
A naive way to keep the system busy is to have the thread set the blocked task
aside and do something else.
This may not be the best strategy in general.
Other tasks may need other locks, and overheads may become to expensive.
An alternative is to adapt a kind of ``priority inheritance'' strategy
where a blocked thread tries to speed up inactive blocking tasks by stealing
them and executing them.
We propose to study this avenue in the context of individual data structures.

A different way for combining the fork-join model with new kinds of data structures
is to execute tasks as ``mini-transactions'',
either through hardware transactions or software.
A task that encounters an obstruction can be rolled back and restarted,
or it can roll back and restart an idle blocking task.
Naturally,
the issues of scheduling, stealing, and contention management become more complex.
There are a number of strategies,
originally developed to ensure progress for software transactional memory,
that can be adapted to our library.
For example, in the \emph{greedy}
contention manager~\cite{Guerraoui:2005:TTT:1073814.1073863}
is based on two rules.
First, when an older task is blocked by a younger task,
the younger task is rolled back to allow the older task to run.
Second, if a younger task is blocked by an active older task,
it waits, but if the older blocking task is itself waiting,
then the older task is rolled back,
thus avoiding chains of waiting transactions.
Although this and other
\emph{contention managers}~~\cite{Spear:2009:CSC:1504176.1504199}
were originally developed in the context of software transactional memory,
we think these approaches can be adapted to be used internally by
individual, highly-concurrent data structures,
in a way transparent to the users of these objects.

The addition of locks and mini-transactions to the work-stealing  model has
theoretical aspects worth exploring.
The work-stealing method attempts to approximate a ``greedy'' scheduler that
keeps threads as busy as possible at each moment of the execution.
It is known that greedy schedulers are 2-competitive with the ideal off-line scheduler,
but it is not known whether these results hold when locks and roll-backs are
added to the model.
As part of our research, we will address such questions.

\section{Memory Management}
\label{sec:memory}
Even for sequential programs,
memory management hazards such as dangling pointers and memory leaks
are notoriously difficult to catch.
Automatic memory management techniques,
such as reference counters or garbage collection,
reduce the threat from such pitfalls,
and provides a useful memory management abstraction for the developer.
Today, automatic memory management is considered an important tool to support
development of large and reliable software systems.
Automatic memory management is embedded in modern languages such as Java and C\#,
and is now widely used used in large software projects.

Memory management remains a challenge for highly-concurrent data structures,
particularly data structures that use fine-grained synchronization.
Before freeing memory that is part of a data structure
(say, a node of a linked list),
a thread must ensure that no other thread can subsequently read or write to
that memory.
(Read-only accesses to freed memory can be problematic if the operating
system removes that page from the local address space.)
With locks, it is relatively easy to ensure that memory is freed safely,
because it is possible to lock out other threads while 
the operation is in progress.
By contrast, in highly-concurrent data structures,
where multiple threads may access the data structure simultaneously,
one thread cannot easily determine whether other threads are accessing
(or poised to access) part of the data structure.


Hardware transactions can also help us rethink storage management for
concurrent data structures implemented in unmanaged  languages like C or
C++.
Researchers have proposed mechanisms such as \emph{hazard pointers}
\cite{Michael:2004:HPS:987524.987595},
and the \emph{repeat offender algorithm}
\cite{Herlihy:2002:ROP:645959.676129}.
Proposals to use \emph{reference counting} to track transient references
include Detlefs et al.~\cite{Detlefs:2001:LRC:383962.384016},
who propose a lock-free reference counting scheme that requires a specialized
\emph{double-compare-and-swap} (DCAS) instruction,
and Herlihy et al.~\cite{Herlihy:2002:DLD:571825.571847},
who replaced DCAS with a conventional single-word compare-and-swap.

These solutions are complex to use,
and may impose unacceptable costs in some contexts.
For example, a read-only method must increment and decrement
reference counts for every object it traverses,
using expensive compare-and-swap calls.
As a result,
many highly-concurrent data structures do their own customized memory
management,
an approach that cannot be sustained. But even these solutions, 
when properly implemented with memory fences, require a very high 
overhead, that is often unacceptable. A better solution for the memory
management of highly-concurrent data structures is in dire need. 

In recent work.~\cite{DBLP:conf/podc/DragojevicHLM11},
PI Herlihy and his coauthors showed how hardware transactions can simplify
memory management based on hazard pointers or the repeat offender algorithm.
Even more recent work suggests that hardware transactions can eliminate the
need for such algorithms in many cases,
making memory management substantially simpler and more efficient
(in the common case).

PI Petrank has done extensive work in memory management for concurrent
systems~\cite{
azag99,
doma00,
doma00b,
doma02,
azat03,
azat03b,
abua04,
petr04,
bara05,
kerm06,
leva06,
paz07a,
pizl07a,
pizl08,
pizl08a,
klio09,
bara10}.
While many existing lock-free algorithms rely on a garbage collector,
this approach is not practical in the context of a C++ library.
Even if it were, insuring that the garbage collector support an 
algorithm that must guarantee progress is not possible with 
current techniques. 
Much effort has been put on making garbage collectors support 
lock-free algorithms, with motivation stemming from practical 
real-time systems to a theoretical desire to solve an open 
problem (e.g.,~\cite{herl92,Detlefs:2001:LRC:383962.384016,pizl07a,pizl08}). The main achievement obtained
in this line of work is that the pauses that the collector imposes on 
program execution are bounded. However, the main obstacle and a major difference from other system support primitives
is that the garbage collection must make continual progress and terminate on time.
Otherwise, the program may get stuck on allocation when the heap becomes full,
and the overall lock-free guarantee fails.
While current, lock-free collectors can guarantee lock-freedom for each
specific program operation, at this point, it is not known how to make them guarantee termination of the collection
(unless they make other compromises).
PI Petrank gave a recent invited talk discussing these
issues~\cite{PetrankTalk}.

A different approach to achieving object reclamation for concurrent
algorithms with progress guarantees is to examine individual 
highly-concurrent data structures and ask
whether a light-weight memory management scheme can be used internally by that
object?
(For example, a concurrent hash table might automatically reclaim its own
unreachable data.)
This is called {\em manual reclamation} within the memory 
management community. 
Lock-free algorithms are easier to create if one assumes garbage collection,
so it is natural to ask whether one can design per-object memory management mechanisms,
more specialized than system-wide full garbage collection,
that provides the functionality needed by a specific data structures.
Can we create good manual memory management schemes that 
can reclaim objects that are not needed any more by the concurrent program? This question is the main target of the second part of this work. 

\section{Merits and Impact}
Both Brown University and the Technion have a strong tradition in
undergraduate research. Students benefit from significant individual
attention and are encouraged to pursue advanced degrees in science.
This project will give undergraduate students at Technion and Brown an
opportunity to participate in a significant and innovative research project.
As undergraduates,
they will have a chance to be involved in this research project at several
levels---reading scientific papers about prior and related work,
designing and implementing library components such as innovated data structures,
designing and running experiments,
developing benchmarks, and presenting their work at relevant conferences and workshops. 

Programmability is the key obstacle preventing effective utilization
of modern multicore architectures.
Systems constructed by aggregating these processors can enable
\emph{parallel execution of thousands of threads}.
Unfortunately, the burden imposed on applications to safely and efficiently
program such systems using classical concurrency abstractions is severe.
Moreover, it is unlikely that compilers can transparently extract sufficient
parallelism for most legacy applications to leverage the degree of
parallelism afforded by these new architectures.

New architectures that provide \emph{hardware transactional memory} offer a
promising alternative to existing synchronization architectures.
Nevertheless, it is far from clear how to exploit these new capabilities in
general-purpose programs.
If this work is successful,
then multicore software will be easier to develop and more efficient,
enabling the parallelism provided by multicore architectures to live up to its promise.

\section{An account of available U.S. and Israeli resources}
This research requires mostly personnel and computing equipment. 
The two PIs have the expertise to lead this research through all its 
stages. Research assistants in the form of graduate students can be
easily recruited in Brown and in the Technion to support this research. 
All relevant personnel have work stations readily available for work
on this project. Larger multicore platforms for testing and measuring 
the resulting library and software is also already available at both 
Brown and the Technion for use of this project. Therefore, the requested
budget focuses on support of graduate student funding 
and  travel to enable 
good collaboration between the groups. 

\section{Previous BSF Grants}
Both PIs have not been supported by a BSF grant before. As we might apply
for the corresponding NSF joint grant, the previous NSF grants of PI Herlihy are detailed below. PI Petrank has not received an NSF grant before. 

%
\section{Results of Prior NSF Support}
Herlihy is a PI for NSF award 0811289, September 2008 - August 2012, \$250000 titled 
\emph{A Unified Open-Source Transactional-Memory Infrastructure}.
The goal of this project is to provide a set of APIs and tools to
support experimentation, evaluation, performance tuning and debugging of
parallel applications written to Transactional Memory APIs.
Recent publications from this project include
%[7, 15, 16, 23, 24, 25, 32 ]
\cite{
DBLP:conf/podc/DragojevicHLM11,
HerlihyK2008boost,DBLP:conf/IEEEpact/HerlihyL09,HerlihyK2008check,KoskinenHdreadlocks, 
Koskinen:2010,DBLP:conf/ppopp/RamadanRHW09}.
Herlihy and his co-PIs have organized a series of workshops
\footnote{\url{http://www.cs.brown.edu/ipp/symposia/ipp41/},
\url{http://www.cs.purdue.edu/news/5-06-10.trans.memory.workshop.htm}}
to consult with industrial partners such as Intel, IBM, SUN (now Oracle), and Microsoft.

Herlihy is also a co-PI of NSF award 0903384,
\emph{Collaborative Research: Energy-Aware Memory Synchronization for Embedded
Multicore Systems},
and NSF award 0830491,
\emph{Topological Methods in Distributed and Concurrent Computation}.
This projects are unrelated to the current proposal.

\section{Collaboration Plan}
The PIs have previously conducted informal collaborations.
For example,
PI Herlihy spent the 2010-2011 academic year on sabbatical at the Technion,
where he served on the thesis committee for one of PI Petrank's students.

The PIs intend to support students to work on areas of mutual interest,
such as adapting data structures and algorithms developed by PI Petrank's prior
work to exploit hardware transactional memory,
and to work on innovative memory management algorithms.
The PIs plan to exchange students,
and to collaborate closely with one another, using mutual visits 
to bring up new ideas and enhance joint progress.



\section{Plan of Operation}
The two main goals stated in the introductions were 
\begin{itemize}
\item
Make data structures based on hardware transactions provide strong progress
guarantees, despite the weak guarantees provided by the hardware.

\item
Improve on memory management for highly-concurrent data structures.
As explained below,
hardware transactions present both a challenge and an opportunity.
\end{itemize}

We plan to start by designing algorithms for various data structures 
(e.g., queues, linked-lists, skip-lists, trees, etc.) 
that are based on hardware transactions but provide strong progress guarantees
in spite of the hardware's weak guarantees. This design work for 
fundamental data structures and algorithms will 
be executed by both parties. 
The PIs, together with their students, will attack these problems one by one. The mutual visits will be used, for exchanging ideas, joint brainstorming, and figuring out ways to promote the research projects.
The obtained data structure designs will be implemented 
by students on both sides, to allow incorporation in the hardware
transaction library effort. 

The memory management 
problems will be addressed simultaneously as an independently motivated 
project, but also according to challenges that will come up during the 
design of the concurrent data structures discussed above. 
Ideas for memory management schemes that will support 
lock-freedom more efficiently and in an easier manner will be
explored for each specific data structure, but also in a more general 
manner. 


\pagebreak
\setcounter{page}{1}
\bibliographystyle{acm}
\renewcommand{\refname}{References Cited}
\bibliography{biblio,gc}
\end{document}
