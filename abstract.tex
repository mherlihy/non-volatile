\paragraph{Overview:}
Most computer systems today rely on main memory that is
\emph{volatile}:
its contents are lost after a crash.
As a result, systems that manage long-lived data must frequently
record their state on non-volatile secondary storage such as disks or SSDs,
often a slow process.
By contrast,
\emph{non-volatile} memory does not lose its contents after a system crash. 
Although NVM is not part of today's standard architectures,
the technology is advancing fast,
and is soon expected to displace volatile DRAM for main memory within
a few years.
NVM is not expected to displace caches and registers,
which will remain volatile for the foreseeable future.
This change in technology will have pervasive effects on how software
systems manage long-lived state.

The proposed research concerns the design, implementation, and testing
of \emph{durable} concurrent data structures and algorithms adapted to NVM.
As noted, caches and registers are expected to remain volatile,
so the state of main memory following a crash may be inconsistent,
as cached regions of the data structure may be lost.
Because concurrent operations may have been in progress at the time of
the crash, untangling their effects can make restoring consistency
even more challenging.
Indeed, careful thought is needed to define what it \emph{should mean}
for a object to be durable.
Finally, support for durable objects will require rethinking how
run-time system mechanisms can support such objects,
including services such as memory management and transactional synchronization.

\vspace*{2mm}
\paragraph{Keywords:} concurrent data structures; non-volatile memory;
memory management.
\vspace*{2mm}

%\vspace*{-5mm}
\paragraph{Intellectual merit:}
Integrating durable data structures with conventional systems and
run-times will require addressing multiple research questions.
First, the very meaning of durability for concurrent data structures
is open to interpretation: 
when an object's state is recovered following a crash,
which operations' effects survive and which are lost?
Which dependencies are preserved, both within a single object, and across objects?
How does durability interact with memory management techniques,
both explicit and automatic?
Is some form of atomic transaction needed to ensure consistency across
multiple durable objects? 

%\vspace*{-5mm}
\paragraph{Broader Impact:}
Society is increasingly dependent on systems that manage long-lived data.
This project has the potential to benefit society by making such systems
more responsive and more energy-efficient.
Modern systems must be fault-tolerant,
able to resume operations quickly following power outages,
crashes, software updates, and other service interruptions. 
An adequate use of non-volatile main memory in place of disks and SSDs, 
which we propose to develop in this proposal 
will result is improved resilience, reduced wait times for both system
backup and recovery, greater reliability,
and lower energy consumption.

