LATEX =  "pdflatex"

.SUFFIXES:
.SUFFIXES: .o .cc .fig .eps .ps .pdf .uu .dvi .tex .j

PAPER := project

all: project.pdf CV-maurice.pdf
project.pdf: project.tex abstract.tex CV-maurice.tex defs.tex intro.tex project.tex \
	evaluation.tex biblio.bib collaboration.tex datamanagement.tex research.tex

RERUNTEX = "(undefined references|Rerun to get)"
RERUNBIB = "No file.*\.bbl|Citation.*undefined"

# Run latex; then bibtex and latex again for missing cites; then latex
# again for changed references.
.tex.pdf:
	$(LATEX) $<
	@echo "Considering bibtex rerun"
	@egrep $(RERUNBIB) $(<:.tex=.log) && (bibtex -min-crossrefs=100 $(<:.tex=); $(LATEX) $<); true
	@echo "Considering latex rerun"
	@egrep $(RERUNTEX) $(<:.tex=.log) && $(LATEX) $<; true
	@egrep -i "(Reference|Citation).*undefined" $*.log ; true

clean:
	-rm -f *~  $(PAPER).pdf *.{aux,bbl,blg,log,bak,out,idx,toc,ind,ilg}


