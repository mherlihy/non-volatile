In the absence of durability concerns,
\emph{linearizability}~\cite{HerlihyW1990} is perhaps the most commonly-used
correctness condition for concurrent objects:
each operation appears to ``take effect'' instantaneously at some point between its 
invocation and response.
Linearizability is  attractive because (unlike, say, sequential consistency),
it is \emph{compositional}:
the composition of two linearizable objects is itself linearizable. This has the
important implication that one can prove linearizability of an object independently. 
There is no need to show correctness for each possible interaction between objects in the system. 

Recently,
Izraelevitz \emph{et al.}~\cite{IMS2016} proposed \emph{durable
linearizability} as a natural way to extend linearizability to durable data
structures. 
Informally,
durable linearizability guarantees that the state of the object following a
crash reflects a consistent subhistory of the operations that actually occurred.
This subhistory includes all operations that completed before the crash,
and may or may not include operations in progress when the crash occurred.
It is not hard to see that durable linearizability is compositional:
the composition of two durably linearizable objects is itself durably
linearizable.
Nevertheless, durable linearizability can be expensive,
requiring frequent \emph{persistence barriers}:
forcing data from volatile caches into non-volatile main memory can take
hundreds of cycles.
Izraelevitz \emph{et al.} describe a systematic way to transform a lock-free
linearizable data structure into a durably linearizable data structure by
associating persistence barriers with almost all memory accesses.

Recognizing that such frequent persistence barriers could be expensive,
Izraelevitz \emph{et al.} propose an alternative, weaker condition,
called \emph{buffered durable linearizability}.
Informally,
this condition guarantees that the state of the object following a
crash reflects a consistent subhistory of the operations that actually
occurred,
but this subhistory \emph{need not} include all operations that completed before the crash.
Buffered durable linearizability is potentially more efficient than its
unbuffered counterpart, because it does not require such frequent persistence fences.

Unfortunately,
buffered durable linearizability is not compositional:
the composition of two buffered durably linearizable objects is itself not
buffered durably linearizable.
For example,
suppose a thread dequeues value $x$ from $p$,
and then enqueues $x$ on $q$,
where $p$ and $q$ are distinct buffered durably linearizable FIFO queues.
Following a crash,
the thread might find two copies of $x$, one in each queue,
while if the composition of the two queues were buffered durably linearizable,
the recovering thread might find $x$ in $p$ but not $q$, or in $q$ but not $p$,
or in neither, but never in both.

Durable objects and libraries are unlikely to be useful if they cannot be composed,
since it is essentially impossible for applications to guarantee invariants
that span multiple objects, or to preserve dependencies between objects.
Thus, such objects are expected to provide 
a linearizable operation, \var{sync()},
that forces a single-object persistence barrier:
a call to an object's \var{sync()} method renders durable all that object's
operations completed before the call,
although operations concurrently with the call may or may not be rendered durable.

Several prior projects have proposed transactional updates to persistent
memory that guarantee failure atomicity for a collection
of persistent data updates all occur or none do across failure
boundaries
\cite{ChakrabartiBB14,CoburnCAGGJS2011,GilesDV15,KolliPSCW2016,LuSS2016,VolosTS2011}.
While these approaches work,
they trade off performance for consistency in the face of failures. 
Transaction runtimes incur significant bookkeeping
overheads to consistently manage transaction metadata.
An interesting alternative strategy to transactional updates is
to build libraries of high-performance persistent data
structures~\cite{HPlabs} that are heavily optimized using ad-hoc
techniques informed by the data structure architecture and semantics.

Other recent projects have worked on designing algorithms that support recovery from crashes
involving non-volatile memory. See for example~\cite{chat15,izra16,dhru14,cobu11,koll16,volo11}). 
As for specific data structure designs for non-volatile memory, two designs for 
trees building on fine-grained locking can be found in~\cite{venk11,yang15}.
Neither directly addresses concurrency definitions or challenges.
They assume certain atomic constructs,
and their evaluation does not address scalability or concurrency issues.
In this project, we propose to explore various implementation for concurrent data structures 
with various synchronization methods. We will concentrate on lock-free data structures and 
on support for transactions on non-volatile memories. 
To the best of our knowledge,
our recent preliminary work on durable queues~\cite{FriedmanHP2017},
discussed in Section~\ref{sec:prelim},
is the first durable lock-free data structure.

A promising first design for allocation and reclamation of memory 
on non-volatile memory is due to Bhandari \emph{et al.}~\cite{BCB2016}.
This allocator supports some concurrent data structure designs,
but it it does not support lock-free data structures.
A design for a lock-free allocator that works well on non-volatile memory 
is an open problem,
as is the question of how to best execute memory management 
even in a lock-based environment needs an extended additional study. 

Several papers have proposes definitions for durability.
The notions of durable linearizability and its variations
are due to Izraelevitz \emph{et. al}~\cite{IMS2016}.
Other definitions have been proposed by Condit \emph{et al.}~\cite{ConditNFILBC2009},
some of which can be implemented only after hardware modifications.
They also design a queue, but it is not lock-free,
relying on a combination of locking and flushes for synchronization.

