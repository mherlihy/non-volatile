Most computer systems today rely on main memory that is \emph{volatile}:
its contents are lost after a crash.
As a result, systems that manage long-lived data must frequently
back up their state on non-volatile secondary storage such as disks or SSDs,
typically a slow and energy-intensive process.
By contrast, \emph{non-volatile} memory (NVM) does not lose its contents after a system crash.
Although NVM is not part of today's standard architectures,
the technology is advancing fast,
and is expected to displace volatile DRAM for main memory within a few years.
Nevertheless, due to hardware constraints,
NVM is not expected to displace caches and registers,
which will remain volatile for the foreseeable future.

This change in technology will have pervasive effects on how software
systems manage long-lived state.
Today, systems that manage long-lived data
(that is, data that must survive a system crash),
must frequently pause to back up the system state to non-volatile secondary storage,
often a slow and energy-intensive process.
If a crash occurs, the data must be read back from the disk and
rendered consistent,
also a slow and energy-intensive process.
Furthermore, because applications typically attempt to reduce the performance overhead 
and the frequency of execution pauses, data is written to hard disk infrequently and 
following a crash, the system is restored to a much earlier time. 
By contract,
an architecture with NVM never needs to pause to back up critical
updates to disk during normal operation,
and crash recovery can proceed directly from (faster) NVM
instead of (much slower) disk. Furthermore, the restored state can be one
that represents a recent time in the application execution. 

While these benefits of NVM are compelling,
realizing those benefits presents substantial challenges.
As noted, caches and registers are expected to remain volatile,
so the state of NVM following a crash may be inconsistent,
requiring non-trivial algorithms to restore the data structure to a consistent state.
Furthermore, most modern data management systems are \emph{concurrent},
implying that multiple threads may have been operating on the data at
the time of the crash.

Informally,
a data object is \emph{durable} if it resides in NVM and its state can
be recovered following a crash.
Restoring consistency to durable objects after a crash raises a
many questions, both practical and theoretical.
For example, modern data structures often grow and shrink dynamically,
allocating and releasing memory on the fly.
Restoring consistency to an object requires therefore restoring
consistency to the underlying memory management system,
whether manual or automatic.
There are unresolved question concerning what it \emph{means}
for an object to be durable.
Following a crash, which operations in progress at the time are preserved,
and which are lost?
Can operations that completed before the crash be lost?
Are dependencies between operations preserved within a single object?
Across multiple objects?


The proposed research will address these issues.
We will focus on (1) the design and implementation of specific durable
concurrent data structures,
(2) the run-time system mechanisms needed to support such objects,
mechanisms such as memory management and transactional synchronizaton,
and (3) the theoretical foundations of durability,
including what it means for an object to be durable
(different applicatons may have different requirements).
In the end, the project will produce a library of useful durable
concurrent data structures,
and guidelines for how to transform current long-lived concurrent data
management systems based on volatile memory and disks into similar
systems based on NVM.

Software platforms at large will need to adjust to this change in the computing platforms,
if they want to make the best use of it. 
As software state can be recovered after a crash, at a low cost, there is a huge potential
for increased resilience in all areas of computer software. The holy grail in this direction 
is to allow a complete system recovery after a crash, where the machine can continue executing its tasks
after a crash (and a recovery) as if nothing happened. The knowledge, obtained in the  
proposed research, is expected to allow a better understanding of how to approach this
larger task. If time allows, or as a follow up project that will follow the current proposal, 
we plan to employ the understanding obtained from building 
resilient concurrent data structures and algorithms to build a fully resilient system. 
This includes extending the operating system, the runtime (in particular and most challenging: 
the automatic memory manager), and the Jitting (Just In Time compilation) process to be resilient 
to crashes. 




